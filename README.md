# retoot

A small bot to mirror a twitter account to a mastodon account

# Run retoot

## Preparation

Checklist:
- `python3` and `virtualenv` installed
- A list of credentials:
 * mastodon api id, secret and token: run `retoot.py` without parameters, the script will help you to get them (check docker command too)
 * twitter consumer key and secret: create a [twitter app](http://apps.twitter.com/)
 * twitter access token and secret: generate a token/secret on your twitter app page

Fill up `env.list` with your credentials

Note: you can setup multiple mirrors, check `env.list`

## Build your virtual environment
```
$ virtualenv -p python3 retoot_env
$ source ./retoot_env/bin/activate
$ python3 setup.py install
```

## Run bot
```
$ set -a
$ source env.list
$ python3 retoot/retoot.py
```

# Run retoot with docker

To get your mastodon's api id, secret and token: we run retoot interactively
```
docker run --rm -it mojo42/retoot
```

Once your `env.list` is filled, you can run retoot using a command like:
```
$ docker run --rm -it --env-file env.list mojo42/retoot
```

# License

> Copyright (c) 2017, Jérôme Jutteau <j.jutteau@gmail.com>
>
> This work is open source software, licensed under the terms of the
> BSD license as described in the LICENSE file in the top-level directory.
