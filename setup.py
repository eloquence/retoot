from setuptools import setup, find_packages
setup(
    name="retoot",
    version="0.1",
    packages=find_packages(),
    install_requires=[line.strip() for line in open('requirements.txt')],
    package_data={
        # If any package contains *.txt or *.rst files, include them:
        '': ['*.txt', '*.rst'],
        # And include any *.msg files found in the 'hello' package, too:
        'hello': ['*.msg'],
    },
    author="Jerome Jutteau",
    author_email="j.jutteau@gmail.com",
    description="A bot which mirror twitter message to mastodon message",
    license="BSD-3-Clause",
    keywords="mastodon, mastodon.social, toot, retoot, twitter",
    url="http://www.gitlab.com/mojo42/retoot",
)
